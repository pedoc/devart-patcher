# DevartPatcher

## 测试目标
```dbforgepostgresql 2.3.237```

## 脱壳(可选步骤)
```die```查壳为```Eazfuscator```，但实际不是。通过使用```de4dot```依次尝试不同的壳，发现其壳为```.NET Reactor```，具体版本未知

```bash
de4dot dbforgepostgresql.exe -p dr4 --strtyp delegate --strtok 060005F5
```

其中 ```060005F5``` 为字符串解密函数```Token```

*不同版本和不同产品，此token可能是不同的。*

## 修改关键点
1. Devart.DbForge.Trial.ProductLicense 静态构造函数
目的：启动时不再显示试用过期弹窗

```csharp
ProductLicense.ShowEvaluationFormMode = ShowEvaluationFormMode.EveryStart;
```
修改为
```csharp
ProductLicense.ShowEvaluationFormMode = ShowEvaluationFormMode.Never
```
其中枚举定义如下：
```
using System;

namespace Devart.DbForge.Trial
{
	// Token: 0x02000091 RID: 145
	public enum ShowEvaluationFormMode
	{
		// Token: 0x040001F7 RID: 503
		Never,
		// Token: 0x040001F8 RID: 504
		EveryStart,
		// Token: 0x040001F9 RID: 505
		WhenEvaluationExpired
	}
}
```

即将 ```ld.i4.1``` 修改为 ```ld.i4.0``` 即可


2. Devart.DbForge.Trial.ProductLicense 字段修改
目的：修改试用信息从而达到无限试用
- EvaluationDaysLeft 硬编码返回99999
- EvaluationCurrent 硬编码返回0
- EvaluationDays 硬编码返回99999
- Licensed 硬编码返回true

*补充mysql9.0.688不要修改EvaluationDaysLeft和Licensed*


## 效果
![](https://images.gitee.com/uploads/images/2021/0923/200635_98bd624d_61974.png "屏幕截图.png")

## 补充
理论上其他产品机制类似，但未做验证
安装时不要选择生成本机程序集

## 字符串解密token收集
|  名称   | 版本  | Token | Cli | 时间| 
|  ----  | ----  |----|-----|-----|
| db forge postgre sql  | 2.3.237 | 060005F5 | de4dot dbforgepostgresql.exe -p dr4 --strtyp delegate --strtok 060005F5|2021-9-23|
| db forge postgre sql  | 2.3.273 | 060005F8 | de4dot dbforgepostgresql.exe -p dr4 --strtyp delegate --strtok 060005F8 |2021-9-23|
